# instructions

## Files Nomenclature
```
"file_name"_test.go
```

## Comands
```
go test -v

go test -cover

go test -coverprofile=coverage.out && go tool cover -html=coverage.out
```