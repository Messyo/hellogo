package main

import "fmt"

func main() {
	fmt.Println("EAE MUNDO DE NOVO")

	var whatToSay string

	whatToSay = "VLW FLW MUNDO"
	var i int

	fmt.Println(whatToSay)

	i = 7

	fmt.Println("i é", i)

	whatWasSaid, theOtherThingThatWasSaid := saySomething()

	fmt.Println("A FUNCAO RETORNOU", whatWasSaid, theOtherThingThatWasSaid)
}

func saySomething() (string, string) {
	return "something", "else"

}
