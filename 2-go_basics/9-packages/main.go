package main

import (
	"log"

	"github.com/messyoxd/myniceprogram/helpers"
)

func main() {
	var myVar helpers.SomeType

	myVar.TypeName = "Some Name"
	log.Println(myVar.TypeName)
}
