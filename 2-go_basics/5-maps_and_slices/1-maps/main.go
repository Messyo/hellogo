package main

import "log"

type User struct {
	FirstName string
	LastName  string
}

func main() {
	// doesnt need pointer to alter the original map
	myMap := make(map[string]User)

	me := User{
		FirstName: "Messyo",
		LastName:  "Sousa",
	}

	myMap["me"] = me

	log.Println(myMap["me"].FirstName)

	var myNewVar float32

	myNewVar = 11.1

	log.Println(myNewVar)
}
