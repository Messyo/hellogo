package main

import "log"

func main() {
	mySlice := []string{"Messyo"}

	mySlice = append(mySlice, "Sousa")
	mySlice = append(mySlice, "Brito")

	log.Println(mySlice)

	numbers := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	log.Println(numbers)
	log.Println(numbers[6:8])
}
