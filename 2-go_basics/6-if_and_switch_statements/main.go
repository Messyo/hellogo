package main

import "log"

func main() {
	myVar := "fish"

	if myVar == "cat" {
		log.Println("myVar is Cat")
	} else {
		log.Println("myVar is not Cat")
	}

	switch myVar {
	case "cat":
		log.Println("myVar is Cat")
	case "fish":
		log.Println("myVar is fish")
	default:
		log.Println("myVar is something idk")
	}

}
